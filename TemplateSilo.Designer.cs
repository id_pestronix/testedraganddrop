﻿namespace testeDragAndDrop
{
    partial class TemplateSilo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSilo = new Guna.UI2.WinForms.Guna2GroupBox();
            this.cBoxSilo = new System.Windows.Forms.ComboBox();
            this.labelKg = new System.Windows.Forms.Label();
            this.labelPesoSilo = new System.Windows.Forms.Label();
            this.probarSilo = new Guna.UI2.WinForms.Guna2VProgressBar();
            this.pictureBoxSilo = new System.Windows.Forms.PictureBox();
            this.groupBoxSilo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSilo)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxSilo
            // 
            this.groupBoxSilo.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSilo.BorderColor = System.Drawing.Color.Black;
            this.groupBoxSilo.BorderRadius = 20;
            this.groupBoxSilo.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.groupBoxSilo.BorderThickness = 0;
            this.groupBoxSilo.Controls.Add(this.cBoxSilo);
            this.groupBoxSilo.Controls.Add(this.labelKg);
            this.groupBoxSilo.Controls.Add(this.labelPesoSilo);
            this.groupBoxSilo.Controls.Add(this.probarSilo);
            this.groupBoxSilo.Controls.Add(this.pictureBoxSilo);
            this.groupBoxSilo.CustomBorderColor = System.Drawing.Color.Transparent;
            this.groupBoxSilo.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.groupBoxSilo.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSilo.ForeColor = System.Drawing.Color.White;
            this.groupBoxSilo.Location = new System.Drawing.Point(3, 2);
            this.groupBoxSilo.Name = "groupBoxSilo";
            this.groupBoxSilo.ShadowDecoration.Parent = this.groupBoxSilo;
            this.groupBoxSilo.Size = new System.Drawing.Size(163, 169);
            this.groupBoxSilo.TabIndex = 93;
            this.groupBoxSilo.Text = "SILO 1 - A";
            this.groupBoxSilo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.groupBoxSilo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            this.groupBoxSilo.UseTransparentBackground = true;
            this.groupBoxSilo.Click += new System.EventHandler(this.groupBoxSilo_Click);
            // 
            // cBoxSilo
            // 
            this.cBoxSilo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.cBoxSilo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cBoxSilo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBoxSilo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cBoxSilo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxSilo.ForeColor = System.Drawing.Color.Black;
            this.cBoxSilo.FormattingEnabled = true;
            this.cBoxSilo.Location = new System.Drawing.Point(53, 47);
            this.cBoxSilo.Name = "cBoxSilo";
            this.cBoxSilo.Size = new System.Drawing.Size(63, 22);
            this.cBoxSilo.TabIndex = 85;
            this.cBoxSilo.Visible = false;
            // 
            // labelKg
            // 
            this.labelKg.AutoSize = true;
            this.labelKg.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKg.ForeColor = System.Drawing.Color.White;
            this.labelKg.Location = new System.Drawing.Point(114, 136);
            this.labelKg.Name = "labelKg";
            this.labelKg.Size = new System.Drawing.Size(41, 29);
            this.labelKg.TabIndex = 83;
            this.labelKg.Text = "kg";
            // 
            // labelPesoSilo
            // 
            this.labelPesoSilo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelPesoSilo.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPesoSilo.ForeColor = System.Drawing.Color.White;
            this.labelPesoSilo.Location = new System.Drawing.Point(3, 133);
            this.labelPesoSilo.Name = "labelPesoSilo";
            this.labelPesoSilo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelPesoSilo.Size = new System.Drawing.Size(113, 34);
            this.labelPesoSilo.TabIndex = 83;
            this.labelPesoSilo.Text = "0";
            this.labelPesoSilo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // probarSilo
            // 
            this.probarSilo.BackColor = System.Drawing.Color.Transparent;
            this.probarSilo.FillColor = System.Drawing.Color.Transparent;
            this.probarSilo.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.probarSilo.Location = new System.Drawing.Point(53, 50);
            this.probarSilo.Maximum = 18200;
            this.probarSilo.Name = "probarSilo";
            this.probarSilo.ProgressColor = System.Drawing.Color.Blue;
            this.probarSilo.ProgressColor2 = System.Drawing.Color.Navy;
            this.probarSilo.ShadowDecoration.Parent = this.probarSilo;
            this.probarSilo.Size = new System.Drawing.Size(63, 51);
            this.probarSilo.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.probarSilo.TabIndex = 84;
            this.probarSilo.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.probarSilo.UseTransparentBackground = true;
            this.probarSilo.Value = 18200;
            // 
            // pictureBoxSilo
            // 
            this.pictureBoxSilo.Image = global::testeDragAndDrop.Properties.Resources.silo_pequeno_trans;
            this.pictureBoxSilo.Location = new System.Drawing.Point(34, 42);
            this.pictureBoxSilo.Name = "pictureBoxSilo";
            this.pictureBoxSilo.Size = new System.Drawing.Size(101, 88);
            this.pictureBoxSilo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSilo.TabIndex = 83;
            this.pictureBoxSilo.TabStop = false;
            // 
            // TemplateSilo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Controls.Add(this.groupBoxSilo);
            this.DoubleBuffered = true;
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Name = "TemplateSilo";
            this.Size = new System.Drawing.Size(173, 172);
            this.groupBoxSilo.ResumeLayout(false);
            this.groupBoxSilo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSilo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2GroupBox groupBoxSilo;
        private System.Windows.Forms.ComboBox cBoxSilo;
        private System.Windows.Forms.Label labelKg;
        private System.Windows.Forms.Label labelPesoSilo;
        private Guna.UI2.WinForms.Guna2VProgressBar probarSilo;
        private System.Windows.Forms.PictureBox pictureBoxSilo;
    }
}
