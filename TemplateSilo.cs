﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;

namespace testeDragAndDrop
{
    public partial class TemplateSilo : UserControl
    {
        //--------------- Segunda Forma ------------------

        #region Segunda Forma

        public Label templateSiloLabelPeso, templateSiloLabelKg;
        public Guna2VProgressBar templateSiloProBar;
        public Guna2GroupBox templateSiloGroupBox;

        private void groupBoxSilo_Click(object sender, EventArgs e)
        {
            cBoxSilo.Visible = !cBoxSilo.Visible;
        }

        public TemplateSilo()
        {
            InitializeComponent();

            templateSiloLabelPeso = labelPesoSilo;
            templateSiloLabelKg = labelKg;
            templateSiloProBar = probarSilo;
            templateSiloGroupBox = groupBoxSilo;
        }

        #endregion Segunda Forma

        //--------------- Segunda Forma ------------------
    }
}