﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2;

namespace testeDragAndDrop
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private Control activeControl;
        private Point previousPosition;
        private int numsilos = 1;

        private void carregarDragSilos(TemplateSilo templateSilo)
        {
            for (int i = 0; i < numsilos + 1; i++)
            {
            }
        }

        //----------------------- primeira maneira --------------------------

        #region primeira maneira

        private void buttonCriarSilo_Click(object sender, EventArgs e)
        {
            var silo = new TemplateSilo();
            silo.Location = new Point(30, 50);
            silo.Size = new Size(171, 174);

            silo.MouseDown += new MouseEventHandler(silo_MouseDown);
            silo.MouseMove += new MouseEventHandler(silo_MouseMove);
            silo.MouseUp += new MouseEventHandler(silo_MouseUp);

            panel1.Controls.Add(silo);
        }

        private void silo_MouseUp(object sender, MouseEventArgs e)
        {
            activeControl = null;
            Cursor = Cursors.Default;
        }

        private void silo_MouseMove(object sender, MouseEventArgs e)
        {
            if (activeControl == null || activeControl != sender)
            {
                return;
            }
            var location = activeControl.Location;

            location.Offset(e.Location.X - previousPosition.X, e.Location.Y - previousPosition.Y);
            activeControl.Location = location;
        }

        private void silo_MouseDown(object sender, MouseEventArgs e)
        {
            activeControl = sender as Control;
            previousPosition = e.Location;
            Cursor = Cursors.Hand;
        }

        private void buttonCriarPanel_Click(object sender, EventArgs e)
        {
            var myControl = new Panel();
            myControl.Location = new Point(200, 200);
            myControl.Size = new Size(25, 25);
            myControl.BackColor = Color.Red;

            myControl.MouseDown += new MouseEventHandler(myControl_MouseDown);
            myControl.MouseMove += new MouseEventHandler(myControl_MouseMove);
            myControl.MouseUp += new MouseEventHandler(myControl_MouseUp);

            panel1.Controls.Add(myControl);
        }

        private void myControl_MouseUp(object sender, MouseEventArgs e)
        {
            activeControl = null;
            Cursor = Cursors.Default;
        }

        private void myControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (activeControl == null || activeControl != sender)
            {
                return;
            }
            var location = activeControl.Location;

            location.Offset(e.Location.X - previousPosition.X, e.Location.Y - previousPosition.Y);
            activeControl.Location = location;
        }

        private void myControl_MouseDown(object sender, MouseEventArgs e)
        {
            activeControl = sender as Control;
            previousPosition = e.Location;
            Cursor = Cursors.Hand;
        }

        #endregion primeira maneira

        //----------------------- primeira maneira --------------------------

        //---------------------- segunda maneira -----------------------------

        #region segunda maneira

        private void buttonCriarSilo2_Click(object sender, EventArgs e)
        {
            int i = 0;
            numsilos++;
            TemplateSilo[] controls = new TemplateSilo[numsilos];

            controls[i] = new TemplateSilo();

            controls[i].templateSiloProBar.Maximum = 20000;
            controls[i].templateSiloProBar.Minimum = 0;
            controls[i].templateSiloProBar.Value = 10000;

            controls[i].templateSiloLabelPeso.Text = "10000";

            // This adds the controls to the form (you will need to specify thier co-ordinates etc. first)

            controls[i].Location = new Point(30, 50);
            controls[i].Size = new Size(171, 174);
            ControlExtension.Draggable(controls[i], true);
            this.Controls.Add(controls[i]);

            i++;
        }

        #endregion segunda maneira

        //---------------------- segunda maneira -----------------------------
    }
}