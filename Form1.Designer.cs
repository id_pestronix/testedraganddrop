﻿namespace testeDragAndDrop
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCriarSilo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCriarPanel = new System.Windows.Forms.Button();
            this.templateSilo1 = new testeDragAndDrop.TemplateSilo();
            this.buttonCriarSilo2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCriarSilo
            // 
            this.buttonCriarSilo.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCriarSilo.Location = new System.Drawing.Point(87, 49);
            this.buttonCriarSilo.Name = "buttonCriarSilo";
            this.buttonCriarSilo.Size = new System.Drawing.Size(235, 68);
            this.buttonCriarSilo.TabIndex = 1;
            this.buttonCriarSilo.Text = "1 Criar Silo";
            this.buttonCriarSilo.UseVisualStyleBackColor = true;
            this.buttonCriarSilo.Click += new System.EventHandler(this.buttonCriarSilo_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Location = new System.Drawing.Point(328, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 751);
            this.panel1.TabIndex = 2;
            // 
            // buttonCriarPanel
            // 
            this.buttonCriarPanel.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCriarPanel.Location = new System.Drawing.Point(87, 322);
            this.buttonCriarPanel.Name = "buttonCriarPanel";
            this.buttonCriarPanel.Size = new System.Drawing.Size(235, 68);
            this.buttonCriarPanel.TabIndex = 3;
            this.buttonCriarPanel.Text = "Criar Panel";
            this.buttonCriarPanel.UseVisualStyleBackColor = true;
            this.buttonCriarPanel.Click += new System.EventHandler(this.buttonCriarPanel_Click);
            // 
            // templateSilo1
            // 
            this.templateSilo1.BackColor = System.Drawing.Color.Gainsboro;
            this.templateSilo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.templateSilo1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.templateSilo1.Location = new System.Drawing.Point(149, 591);
            this.templateSilo1.Name = "templateSilo1";
            this.templateSilo1.Size = new System.Drawing.Size(173, 172);
            this.templateSilo1.TabIndex = 4;
            // 
            // buttonCriarSilo2
            // 
            this.buttonCriarSilo2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCriarSilo2.Location = new System.Drawing.Point(87, 123);
            this.buttonCriarSilo2.Name = "buttonCriarSilo2";
            this.buttonCriarSilo2.Size = new System.Drawing.Size(235, 68);
            this.buttonCriarSilo2.TabIndex = 5;
            this.buttonCriarSilo2.Text = "2 Criar Silo";
            this.buttonCriarSilo2.UseVisualStyleBackColor = true;
            this.buttonCriarSilo2.Click += new System.EventHandler(this.buttonCriarSilo2_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1251, 775);
            this.Controls.Add(this.buttonCriarSilo2);
            this.Controls.Add(this.templateSilo1);
            this.Controls.Add(this.buttonCriarPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonCriarSilo);
            this.Name = "FormMain";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonCriarSilo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCriarPanel;
        private TemplateSilo templateSilo1;
        private System.Windows.Forms.Button buttonCriarSilo2;
    }
}

